import React from "react";
import ButtonOption from "../Button/ButtonOption/ButtonOption";

const ControlButtons = (props) => {
  const { currentData, func } = props;
  const { fillData, resetLatestData, clearAllData, saveData } = func;
  const displayFillData =
    currentData?.length > 0
      ? currentData.some((item) => !!item.className)
      : false;
  return (
    <div>
      {displayFillData && (
        <ButtonOption handleOnClick={fillData}>Fill data</ButtonOption>
      )}
      <ButtonOption handleOnClick={resetLatestData}>Reset data</ButtonOption>
      <ButtonOption handleOnClick={clearAllData}>Clear all data</ButtonOption>
      <ButtonOption handleOnClick={saveData}>Save Data</ButtonOption>
    </div>
  );
};

export default ControlButtons;
