import React from "react";
import "./ControlButtons.css";
import ButtonOption from "../Button/ButtonOption/ButtonOption";

const ControlButtons = (props) => {
  const { currentData, func } = props;
  const { fillData, resetLatestData, clearAllData, saveData } = func;
  const displayFillData =
    currentData?.length > 0
      ? currentData.some((item) => !!item.className)
      : false;
  return (
    <div className="bp_control_buttons">
      <div>
        {displayFillData && (
          <ButtonOption handleOnClick={fillData} isControl>
            Fill
          </ButtonOption>
        )}
      </div>
      <div>
        <ButtonOption handleOnClick={resetLatestData} isControl>
          Reset
        </ButtonOption>
      </div>
      <div>
        <ButtonOption handleOnClick={clearAllData} isControl>
          Clear all
        </ButtonOption>
      </div>
      <div>
        <ButtonOption handleOnClick={saveData} isControl>
          Save
        </ButtonOption>
      </div>
    </div>
  );
};

export default ControlButtons;
