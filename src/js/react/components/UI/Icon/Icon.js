import React from "react";
import ForwardIcon from "../../../../../img/forward.png";
import ClearIcon from "../../../../../img/delete.png";
import Constants from "../../../../constants/constant.json";

const iconSrc = {
  [Constants.ICON_TYPES.FORWARD]: ForwardIcon,
  [Constants.ICON_TYPES.CLEAR]: ClearIcon,
};

export default function Icon({ type }) {
  return (
    <div className="bp_icon">
      <p>{iconSrc[type]}</p>
      <img src={ForwardIcon} />
    </div>
  );
}
