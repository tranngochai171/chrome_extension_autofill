import React from "react";
import { FaSearch } from "react-icons/fa";
import "./Search.css";

const search = ({ searchKey, func }) => {
  return (
    <div className="bp__wrapper">
      <span className="bp__search__icon">
        <FaSearch />
      </span>
      <input
        type="text"
        className="bp__search"
        value={searchKey}
        onChange={(e) => func.onChangeSearch(e.target.value)}
        placeholder="Search by Name"
      />
    </div>
  );
};

export default search;
