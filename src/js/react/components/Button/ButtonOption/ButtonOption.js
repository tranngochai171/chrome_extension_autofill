import React from "react";
import "./ButtonOption.css";
const buttonOption = (props) => {
  return (
    <button
      className={`cb_option_btn ${props.isControl ? "control" : ""}`}
      onClick={props.handleOnClick}
    >
      {props.children}
    </button>
  );
};

export default buttonOption;
