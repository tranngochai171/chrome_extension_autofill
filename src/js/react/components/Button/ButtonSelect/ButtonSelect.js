import React from "react";
import "./ButtonSelect.css";
import commonFunction from "../../../../functions/commonFunction";
const buttonSelect = (props) => {
  const { data } = props;
  return (
    <button
      className={`cb_btn ${commonFunction.getBtnSelectClassName(data?.status)}`}
      onClick={props.onChangeStatusSelectBtn}
    >
      {props.children}
    </button>
  );
};

export default buttonSelect;
