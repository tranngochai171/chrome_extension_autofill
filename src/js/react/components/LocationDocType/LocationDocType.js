import React from "react";
import "./LocationDocType.css";

const locationDocType = (props) => {
  const { state, func } = props;
  let renderDoctypeSelection = null;
  if (state.__LOC_ID) {
    const arrOption =
      state.__LIST_DOC[
        state.__LIST_DOC.findIndex((item) => item.loc_id === state.__LOC_ID)
      ].list_docs;
    renderDoctypeSelection = (
      <>
        <div>
          <label htmlFor="bp__select">Document Type:</label>
        </div>
        <div>
          <select
            className="bp__select"
            id="bp__select"
            name="docType"
            disabled={!!state.currentData || state.isLoading}
            onChange={(e) => {
              func.changeDoc(e.target.value);
            }}
          >
            {arrOption.map((item, index) => (
              <option key={index} value={item.doc_tp_id}>
                {item.doc_nm}
              </option>
            ))}
          </select>
        </div>
      </>
    );
  }
  return (
    <div className="bp__select__section">
      <div>
        <div>
          <label htmlFor="bp__select">Location:</label>
        </div>
        <div>
          <select
            className="bp__select"
            id="bp__select"
            name="locationName"
            disabled={!!state.currentData || state.isLoading}
            onChange={(e) => {
              func.changeLocation(e.target.value);
            }}
          >
            {state.__LIST_DOC &&
              state.__LIST_DOC.map((item, index) => (
                <option key={index} value={item.loc_id}>
                  {item.loc_nm}
                </option>
              ))}
          </select>
        </div>
      </div>
      <div>{renderDoctypeSelection}</div>
    </div>
  );
};

export default locationDocType;
