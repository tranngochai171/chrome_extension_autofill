import React from "react";
import "./ImportButtons.css";
import ButtonSelect from "../Button/ButtonSelect/ButtonSelect";
import ButtonOption from "../Button/ButtonOption/ButtonOption";
import { RiShareForwardFill } from "react-icons/ri";
import { BsTrash } from "react-icons/bs";

const ImportButtons = (props) => {
  const { currentData, func } = props;
  const { changeSelectBtnStatus, forwardOrClearInput } = func;
  const renderData = () => {
    return currentData.map((item, index) => {
      let render = null;
      if (item.display) {
        render = (
          <tr key={index}>
            <td>
              <ButtonSelect
                data={item}
                onChangeStatusSelectBtn={() => changeSelectBtnStatus(index)}
              >
                {item.key}
              </ButtonSelect>
            </td>
            <td>
              {item.className && (
                <>
                  <ButtonOption
                    handleOnClick={() =>
                      forwardOrClearInput(index, true /* isForwardFunc */)
                    }
                  >
                    <RiShareForwardFill />
                  </ButtonOption>
                  <ButtonOption
                    handleOnClick={() =>
                      forwardOrClearInput(index, false /* isForwardFunc */)
                    }
                  >
                    <BsTrash />
                  </ButtonOption>
                </>
              )}
            </td>
          </tr>
        );
      }
      return render;
    });
  };
  return (
    <div id="bp__box">
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderData()}</tbody>
      </table>
    </div>
  );
};

export default ImportButtons;
