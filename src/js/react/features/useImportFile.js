import { useState, useReducer, useEffect } from "react";
import axios from "../../functions/axios";
import fireBaseAxios from "../../functions/firebaseAxios";
import Constants from "../../constants/constant.json";
import getOptionAxios from "../../functions/optionAxios";
import commonFunction from "../../functions/commonFunction";

const updateObject = (oldState, objectUpdate) => ({
  ...oldState,
  ...objectUpdate,
});

const actionTypes = {
  FETCH_INIT: "FETCH_INIT",
  FETCH_SUCCESS: "FETCH_SUCCESS",
  FETCH_FAIL: "FETCH_FAIL",
  SET_FIREBASE_KEY_NAME: "SET_FIREBASE_KEY_NAME",
  SET_FIREBASE_DATA: "SET_FIREBASE_DATA",
  SET_CURRENT_DATA: "SET_CURRENT_DATA",
  SET_PLAIN_DATA_FROM_BE: "SET_PLAIN_DATA_FROM_BE",
  RESET_ALL_DATA: "RESET_ALL_DATA",
  SET_SEARCH_KEY: "SET_SEARCH_KEY",
  SET_LIST_DOC: "SET_LIST_DOC",
  SET_LOCATION: "SET_LOCATION",
  SET_DOCTYPE: "SET_DOCTYPE",
};

const actionCreators = {
  fetchInit: () => ({
    type: actionTypes.FETCH_INIT,
  }),
  fetchSuccess: () => ({
    type: actionTypes.FETCH_SUCCESS,
  }),
  fetchFail: () => ({
    type: actionTypes.FETCH_FAIL,
  }),
  setFireBaseKeyName: (keyName) => ({
    type: actionTypes.SET_FIREBASE_KEY_NAME,
    payload: keyName,
  }),
  setDocType: (docType) => ({
    type: actionTypes.SET_DOCTYPE,
    payload: docType,
  }),
  setFireBaseData: (data) => ({
    type: actionTypes.SET_FIREBASE_DATA,
    payload: data,
  }),
  setCurrentData: (data) => ({
    type: actionTypes.SET_CURRENT_DATA,
    payload: data,
  }),
  setPlainDataFromBe: (data) => ({
    type: actionTypes.SET_PLAIN_DATA_FROM_BE,
    payload: data,
  }),
  resetAllData: () => ({
    type: actionTypes.RESET_ALL_DATA,
  }),
  setSearchKey: (data) => ({
    type: actionTypes.SET_SEARCH_KEY,
    payload: data,
  }),
  setListDoc: (data) => ({
    type: actionTypes.SET_LIST_DOC,
    payload: data,
  }),
  setLocation: (data) => ({
    type: actionTypes.SET_LOCATION,
    payload: data,
  }),
};

const reducer = (state, { type, payload }) => {
  switch (type) {
    case actionTypes.FETCH_INIT:
      return updateObject(state, { isLoading: true, isError: false });
    case actionTypes.FETCH_SUCCESS:
      return updateObject(state, {
        isLoading: false,
        isError: false,
      });
    case actionTypes.FETCH_FAIL:
      return updateObject(state, { isLoading: false, isError: true });
    case actionTypes.SET_FIREBASE_KEY_NAME:
      return updateObject(state, { __FIREBASE_KEY_NAME: payload });
    case actionTypes.SET_FIREBASE_DATA:
      return updateObject(state, { __FIREBASE_DATA: payload });
    case actionTypes.SET_PLAIN_DATA_FROM_BE:
      return updateObject(state, { __PLAIN_DATA_FROM_BE: payload });
    case actionTypes.SET_CURRENT_DATA:
      return updateObject(state, { currentData: payload });
    case actionTypes.RESET_ALL_DATA:
      return updateObject(state, {
        currentData: null,
        searchKey: "",
        __PLAIN_DATA_FROM_BE: null,
        __FIREBASE_DATA: null,
        __FIREBASE_KEY_NAME: null,
      });
    case actionTypes.SET_DOCTYPE: {
      return updateObject(state, { __DOCTYPE_ID: payload });
    }
    case actionTypes.SET_SEARCH_KEY: {
      return updateObject(state, { searchKey: payload });
    }
    case actionTypes.SET_LIST_DOC: {
      return updateObject(state, { __LIST_DOC: payload });
    }
    case actionTypes.SET_LOCATION: {
      return updateObject(state, { __LOC_ID: payload });
    }
    default:
      return state;
  }
};

async function getDataFromFireBase(userId, loc_id, docType, token, dispatch) {
  try {
    let isError = false;
    let list = null;
    const res = await fireBaseAxios(
      getOptionAxios.getOptionCallAPI(
        "get",
        `/api/xtn/cfg?usr_id=${userId}&loc_cd=${loc_id}&doc_tp_id=${docType}&site_url=${window.location.href}`,
        null,
        token
      )
    );
    list = res.data.data[0].cfg_val;
    dispatch(actionCreators.setFireBaseKeyName(res.data.data[0].xtn_cfg_id));
    return { list, isError };
  } catch (err) {
    return { err, isError: true };
  }
}

const useImportFile = (token = null, user = null) => {
  const [formData, setFormData] = useState("");
  const [state, dispatch] = useReducer(reducer, {
    isLoading: false,
    isError: false,
    currentData: null,
    searchKey: "",
    __PLAIN_DATA_FROM_BE: null,
    __FIREBASE_DATA: null,
    __FIREBASE_KEY_NAME: null,
    __LIST_DOC: null,
    __LOC_ID: null,
    __DOCTYPE_ID: null,
    __AUTH_TOKEN: token,
    __USER: user,
  });

  useEffect(() => {
    const getDocList = () => {
      chrome.storage.sync.get([Constants.DOC_TYPES], (item) => {
        if (item.DOC_TYPES.length > 0) {
          dispatch(actionCreators.setListDoc(item.DOC_TYPES));
          dispatch(actionCreators.setLocation(item.DOC_TYPES[0].loc_id));
          dispatch(
            actionCreators.setDocType(item.DOC_TYPES[0].list_docs[0].doc_tp_id)
          );
        }
      });
    };
    getDocList();
  }, []);

  const changeSelectBtnStatus = (index) => {
    const currentData = JSON.parse(JSON.stringify(state.currentData));
    const currentBtnSelectedIndex = commonFunction.findIndexOfCurrentSelectedBtn(
      currentData
    );
    if (currentBtnSelectedIndex === -1) {
      // Chưa cái nào chọn
      currentData[index].status = Constants.BTN_STATUS.CURRENT_SELECT;
    } else if (index === currentBtnSelectedIndex) {
      // Chọn lại chính nó
      currentData[index].status = currentData[index].className
        ? Constants.BTN_STATUS.SELECTED
        : Constants.BTN_STATUS.NOT_SELECT;
    } else {
      // Cái khác đang chọn
      currentData[currentBtnSelectedIndex].status = currentData[
        currentBtnSelectedIndex
      ].className
        ? Constants.BTN_STATUS.SELECTED
        : Constants.BTN_STATUS.NOT_SELECT;
      currentData[index].status = Constants.BTN_STATUS.CURRENT_SELECT;
    }
    dispatch(actionCreators.setCurrentData(currentData));
  };

  const onChangeSearch = (value) => {
    dispatch(actionCreators.setSearchKey(value));
    let currentDataTemp = JSON.parse(JSON.stringify(state.currentData));
    let search = value.toLowerCase().trim();
    let regex = new RegExp(".*" + search + ".*", "g");
    const flag = !search;
    for (let i = 0; i < currentDataTemp.length; i++) {
      currentDataTemp[i].display = flag || regex.test(currentDataTemp[i].key);
    }
    dispatch(actionCreators.setCurrentData(currentDataTemp));
  };

  const resetLatestData = () => {
    if (state.__FIREBASE_DATA) {
      dispatch(actionCreators.setCurrentData(state.__FIREBASE_DATA));
    }
  };

  const clearAllData = () => {
    dispatch(actionCreators.setCurrentData(state.__PLAIN_DATA_FROM_BE));
    dispatch(actionCreators.setSearchKey(""));
  };

  const saveData = async () => {
    let cfg_val = JSON.parse(JSON.stringify(state.currentData));
    cfg_val = cfg_val.map((item) => {
      delete item.value;
      delete item.display;
      return item;
    });
    const data = {
      usr_id: state.__USER.usrId,
      loc_cd: state.__LOC_ID,
      doc_tp_id: state.__DOCTYPE_ID,
      site_url: window.location.href,
      cfg_val,
    };
    let res = null;
    try {
      dispatch(actionCreators.fetchInit());
      if (state.__FIREBASE_KEY_NAME) {
        res = await fireBaseAxios(
          getOptionAxios.getOptionCallAPI(
            "put",
            `/api/xtn/cfg/${state.__FIREBASE_KEY_NAME}`,
            data,
            state.__AUTH_TOKEN
          )
        );
      } else {
        res = await fireBaseAxios(
          getOptionAxios.getOptionCallAPI(
            "post",
            "/api/xtn/cfg",
            data,
            state.__AUTH_TOKEN
          )
        );
      }
      if (res.status === 200) {
        dispatch(actionCreators.fetchSuccess());
        alert("Save import data successfully");
        dispatch(
          actionCreators.setFireBaseData(
            JSON.parse(JSON.stringify(state.currentData))
          )
        );
        dispatch(actionCreators.setFireBaseKeyName(res.data.xtn_cfg_id));
      } else {
        throw new Error("error");
      }
    } catch (error) {
      dispatch(actionCreators.fetchFail());
    }
  };

  const fillData = () => {
    for (let i = 0; i < state.currentData.length; i++) {
      const item = state.currentData[i];
      if (item.className) {
        const selectElement = document.getElementsByClassName(
          item.className
        )[0];
        if (selectElement.disabled || selectElement.readOnly) {
          continue;
        }
        if (
          selectElement instanceof HTMLSelectElement &&
          !commonFunction.findValueInOptions(selectElement.options, item.value)
        ) {
          continue;
        }
        selectElement.value = item.value;
        selectElement.dispatchEvent(new Event("change", { bubbles: true }));
      }
    }
  };

  const handleEvent = (e) => {
    if (e.target.disabled || e.target.readOnly) {
      return;
    }

    const currentData = JSON.parse(JSON.stringify(state.currentData));
    if (!currentData) {
      return;
    }
    const currentBtnSelectedIndex = commonFunction.findIndexOfCurrentSelectedBtn(
      currentData
    );
    if (currentBtnSelectedIndex !== -1) {
      const classNameInput = commonFunction.getInputClassName(
        e.target.classList
      );
      if (currentData[currentBtnSelectedIndex].className === classNameInput) {
        // fill chính cái đã chọn
        currentData[currentBtnSelectedIndex].status =
          Constants.BTN_STATUS.SELECTED;
      } else {
        const index = commonFunction.getIndexOfClassNameAlreadyExist(
          currentData,
          classNameInput
        );
        if (
          e.target instanceof HTMLSelectElement &&
          !commonFunction.findValueInOptions(
            e.target.options,
            currentData[currentBtnSelectedIndex].value
          )
        ) {
          return;
        }
        if (index > -1) {
          // fill input đã được btn khác chọn
          if (
            confirm(
              "This input is already filled by another button. Do you want to override it?"
            )
          ) {
            delete currentData[index].className;
            currentData[index].status = Constants.BTN_STATUS.NOT_SELECT;
          } else {
            return;
          }
        }
        currentData[currentBtnSelectedIndex].className = classNameInput;
        currentData[currentBtnSelectedIndex].status =
          Constants.BTN_STATUS.SELECTED;
        e.target.value = currentData[currentBtnSelectedIndex].value;
        e.target.dispatchEvent(new Event("change", { bubbles: true }));
      }
      dispatch(actionCreators.setCurrentData(currentData));
    }
  };

  const addListenerForDataInput = () => {
    const inputElements = document.getElementsByClassName("cb_import");
    for (var i = 0; i < inputElements.length; i++) {
      inputElements[i].addEventListener("click", handleEvent);
    }
  };

  const clearListenerForDataInput = () => {
    const inputElements = document.getElementsByClassName("cb_import");
    for (var i = 0; i < inputElements.length; i++) {
      inputElements[i].removeEventListener("click", handleEvent);
    }
  };

  const forwardOrClearInput = (index, isForwardFunc) => {
    const currentData = JSON.parse(JSON.stringify(state.currentData));
    const importClassName = currentData[index]?.className;
    if (importClassName) {
      const selectElement = document.getElementsByClassName(importClassName)[0];
      if (selectElement.disabled || selectElement.readOnly) {
        return;
      }
      if (isForwardFunc) {
        selectElement.scrollIntoView();
        selectElement.focus();
        if (
          selectElement instanceof HTMLSelectElement &&
          !commonFunction.findValueInOptions(
            selectElement.options,
            currentData[index].value
          )
        ) {
          return;
        }
        selectElement.value = currentData[index].value;
      } else {
        delete currentData[index].className;
        currentData[index].status = Constants.BTN_STATUS.NOT_SELECT;
        dispatch(actionCreators.setCurrentData(currentData));
        if (selectElement instanceof HTMLSelectElement) {
          return;
        }
        selectElement.value = "";
      }
      selectElement.dispatchEvent(new Event("change", { bubbles: true }));
    }
  };

  const resetAll = () => {
    dispatch(actionCreators.resetAllData());
  };

  const changeLocation = (newLocationId) => {
    dispatch(actionCreators.setLocation(newLocationId));
    dispatch(
      actionCreators.setDocType(
        state.__LIST_DOC[
          state.__LIST_DOC.findIndex((item) => item.loc_id === newLocationId)
        ].list_docs[0].doc_tp_id
      )
    );
  };

  const changeDoc = (newDocId) => {
    dispatch(actionCreators.setDocType(newDocId));
  };

  useEffect(() => {
    addListenerForDataInput();
    return () => {
      clearListenerForDataInput();
    };
  }, [state.currentData]);

  useEffect(() => {
    if (formData) {
      const fetchData = async () => {
        let __CURRENT_FILE_NAME = "";
        let __CURRENT_FILE_PRE_PROCESSED = "";
        let __CURRENT_TEMPLATE_TYPE = "";
        let __CURRENT_TEMPLATE_ID = "";
        let __ORG_EXTRACTED_DATA = {};
        let COUNTRY = "TH";
        let UPLOAD_TOKEN = "";
        dispatch(actionCreators.fetchInit());
        try {
          const [
            getUploadToken,
            uploadError,
          ] = await commonFunction.handleAsync(
            axios({
              method: "post", //you can set what request you want to be
              url: Constants.ENDPOINT.AUTH_EXTRACT_FILE,
              data: {
                user_name: "vinhvo",
                password: "123789",
              },
              headers: {
                COUNTRY,
              },
            })
          );
          UPLOAD_TOKEN = getUploadToken.data.Authorization;
          const dataUploading = await axios(
            getOptionAxios.postOption(
              Constants.ENDPOINT.UPLOADING,
              formData,
              UPLOAD_TOKEN,
              COUNTRY
            )
          );
          console.log("uploading", dataUploading.data);
          __CURRENT_FILE_NAME = dataUploading.data.data.data["file_name"];
          const dataPreprocess = await axios(
            getOptionAxios.postOption(
              Constants.ENDPOINT.PRE_PROCESS,
              JSON.stringify({ file_name: __CURRENT_FILE_NAME }),
              UPLOAD_TOKEN,
              COUNTRY
            )
          );
          console.log("preprocess", dataPreprocess.data);
          __CURRENT_FILE_PRE_PROCESSED =
            dataPreprocess.data.data.pre_processed_data;
          const dataMatching = await axios(
            getOptionAxios.postOption(
              Constants.ENDPOINT.MATCHING,
              JSON.stringify({
                file_name: __CURRENT_FILE_NAME,
                pre_processed_data: __CURRENT_FILE_PRE_PROCESSED,
              }),
              UPLOAD_TOKEN,
              COUNTRY
            )
          );
          console.log("matching", dataMatching.data);
          __CURRENT_TEMPLATE_TYPE = dataMatching.data.template_type;
          __CURRENT_TEMPLATE_ID = dataMatching.data.template_id;
          if (dataMatching.data.status_code === 200) {
            const dataExtraction = await axios(
              getOptionAxios.postOption(
                Constants.ENDPOINT.EXTRACTION,
                JSON.stringify({
                  file_name: __CURRENT_FILE_NAME,
                  template_type: __CURRENT_TEMPLATE_TYPE,
                  template_id: __CURRENT_TEMPLATE_ID,
                  pre_processed_data: __CURRENT_FILE_PRE_PROCESSED,
                }),
                UPLOAD_TOKEN,
                COUNTRY
              )
            );
            console.log("extraction", dataExtraction.data);
            __ORG_EXTRACTED_DATA = dataExtraction.data.data;
            const tempJson = [];
            const fieldData = __ORG_EXTRACTED_DATA["fields_data"];
            for (let key in fieldData) {
              if (fieldData[key]) {
                tempJson.push({
                  key,
                  value: fieldData[key],
                  status: Constants.BTN_STATUS.NOT_SELECT,
                  display: true,
                });
              }
            }
            dispatch(
              actionCreators.setPlainDataFromBe(
                JSON.parse(JSON.stringify(tempJson))
              )
            );

            let data = await getDataFromFireBase(
              state.__USER.usrId,
              state.__LOC_ID,
              state.__DOCTYPE_ID,
              state.__AUTH_TOKEN,
              dispatch
            );
            if (data.isError) {
              dispatch(actionCreators.setCurrentData(tempJson));
            } else {
              const list = commonFunction.mapListToList(
                JSON.parse(data.list),
                tempJson
              );
              dispatch(actionCreators.setCurrentData(list));
              dispatch(actionCreators.setFireBaseData(list));
            }
            dispatch(actionCreators.fetchSuccess());
          } else {
            dispatch(actionCreators.fetchFail());
            throw new Error("error");
          }
        } catch (error) {
          dispatch(actionCreators.fetchFail());
          console.log(error);
        }
      };
      fetchData();
    }
  }, [formData]);

  return [
    state,
    setFormData,
    {
      changeSelectBtnStatus,
      resetLatestData,
      clearAllData,
      saveData,
      fillData,
      forwardOrClearInput,
      resetAll,
      onChangeSearch,
      changeLocation,
      changeDoc,
    },
  ];
};

export default useImportFile;
