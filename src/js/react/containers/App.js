import React, { useState, useRef } from "react";
import ControlButtons from "../components/ControlButtons/ControlButtons";
import Search from "../components/Search/Search";
import ImportButtons from "../components/ImportButtons/ImportButtons";
import Spinner from "../components/UI/Spinner/Spinner";
import useImportFile from "../features/useImportFile";
import commonFunction from "../../functions/commonFunction";
import LocationDocType from "../components/LocationDocType/LocationDocType";
import { BiImport } from "react-icons/bi";
import "./App.css";
import "../../../css/content_script.css";

const App = ({ token, user }) => {
  const [state, setFormData, controlFunc] = useImportFile(token, user);
  const [disableUploadBtn, setDisableUploadBtn] = useState(true);
  const inputUploadRef = useRef();
  const handleChangeFile = (e) => {
    const [file] = e.target.files;
    if (file) {
      let { name: fileName, size } = file;
      fileName = commonFunction.wrapText(fileName);
      const fileSize = (size / 1000).toFixed(2);
      const fileNameAndSize = `${fileName} - ${fileSize}KB`;
      document.querySelector("#label-input").textContent = fileNameAndSize;
      setDisableUploadBtn(false);
    }
  };
  const handleUploadFile = () => {
    let file = inputUploadRef.current.files[0];
    let fd = new FormData();
    fd.append("file", file);
    setFormData(fd);
  };
  let renderButtons = null;
  if (state?.currentData?.length > 0) {
    renderButtons = (
      <>
        <hr className="bp__line" />
        <ControlButtons func={controlFunc} currentData={state.currentData} />
        <Search func={controlFunc} searchKey={state.searchKey} />
        <ImportButtons func={controlFunc} currentData={state.currentData} />
      </>
    );
  }

  if (state.isError) {
    renderButtons = (
      <h2 className="bp__error__message">
        Something went wrong! Please try again
      </h2>
    );
  }

  return (
    <div>
      <div id="btn-group-before-import">
        <LocationDocType state={state} func={controlFunc} />
        <div className="bp__flex">
          <div className="bp__file__import">
            <input
              type="file"
              name="file"
              id="file"
              className="import_btn"
              onChange={handleChangeFile}
              accept=".xls, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
              ref={inputUploadRef}
              disabled={!!state.currentData || state.isLoading}
            />
            <label htmlFor="file" id="label-input">
              <span className="bp__import_icon">
                <BiImport />
              </span>{" "}
              Select file
            </label>
          </div>
          {state.currentData ? (
            <button className="close_cb_x" onClick={controlFunc.resetAll}>
              x
            </button>
          ) : (
            <button
              className="bp__upload__btn"
              disabled={disableUploadBtn || state.isLoading}
              onClick={handleUploadFile}
            >
              Upload
            </button>
          )}
        </div>
      </div>
      {state.isLoading ? <Spinner /> : renderButtons}
    </div>
  );
};

export default App;
