import "../css/popup.css";
import axios from "./functions/firebaseAxios";
import Constants from "./constants/constant.json";
const usernameInput = document.getElementById("username");
const passwordInput = document.getElementById("password");
import loginUserAxios from "./functions/loginAxios";
// const countrySelect = document.getElementById("country");
const username_error = document.getElementById("username_war");
const password_error = document.getElementById("password_war");
import commonFunction from "./functions/commonFunction";
require("babel-core/register");
require("babel-polyfill");

const showError = (message) => {
  document.getElementById("common_war").innerHTML = message;
  username_error.innerHTML = "";
  password_error.innerHTML = "";
  usernameInput.value = "";
  passwordInput.value = "";
};

const openApp = () => {
  chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {
      todo: Constants.TODO.TOGGLE_DRAGGABLE_DIV,
    });
    window.close();
  });
};

chrome.storage.sync.get(
  [Constants.AUTH_TOKEN, Constants.EXPIRATION_DATE],
  (item) => {
    const token = item.AUTH_TOKEN;
    if (!token) {
      chrome.storage.sync.clear();
    } else {
      const expirationDate = new Date(JSON.parse(item.EXPIRATION_DATE));
      if (expirationDate <= new Date()) {
        chrome.storage.sync.clear();
      } else {
        openApp();
      }
    }
  }
);

function checkInputValid() {
  let isAllow = true;
  if (!usernameInput.value) {
    username_error.innerHTML = "Please insert user name or email!";
    isAllow = false;
  }
  if (!passwordInput.value) {
    password_error.innerHTML = "Please insert your password!";
    isAllow = false;
  }
  return isAllow;
}

document.getElementById("submit").addEventListener("click", async () => {
  if (checkInputValid()) {
    try {
      const [resAuth, authError] = await commonFunction.handleAsync(
        // loginUserAxios('/api/login', {usrId: usernameInput.value, usrPwd: passwordInput.value})
        loginUserAxios({
          method: "post", //you can set what request you want to be
          url: Constants.ENDPOINT.AUTH_LOGIN_USER,
          data: {
            usrId: usernameInput.value,
            usrPwd: passwordInput.value,
          },
        })
      );
      if (authError) {
        throw { message: "Username/Email or password invalid!" };
      }
      if (resAuth.status === 200) {
        const [listDoc, listDocError] = await commonFunction.handleAsync(
          axios({
            method: "get",
            url: `api/xtn/doc?co_cd=${resAuth.data.userInfo.coCd}`,
            headers: {
              Authorization: "Bearer " + resAuth.data.token,
            },
          })
        );
        if (listDocError) {
          throw {
            message:
              "Something goes wrong on getting Document Type. Please try again.",
          };
        }
        const expirationDate = new Date(
          new Date().getTime() + Constants.EXPIRES_IN * 1000
        );

        chrome.storage.sync.set(
          {
            [Constants.AUTH_TOKEN]: resAuth.data.token,
            [Constants.USER]: resAuth.data.userInfo,
            [Constants.DOC_TYPES]: listDoc.data?.data, // todo
            [Constants.EXPIRATION_DATE]: JSON.stringify(expirationDate),
          },
          function () {
            openApp();
          }
        );
      }
    } catch (error) {
      if (error.message) {
        showError(error.message);
      }
    }
  }
});
