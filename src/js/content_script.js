import Constants from "./constants/constant.json";
import "../css/content_script.css";
import Draggable from "./functions/draggable";

import React from "react";
import ReactDOM from "react-dom";
import App from "./react/containers/App.js";
import commonFunction from "./functions/commonFunction";

require("babel-core/register");
require("babel-polyfill");

function createDraggableDiv() {
  chrome.storage.sync.get(
    [Constants.EXPIRATION_DATE, Constants.AUTH_TOKEN, Constants.USER],
    (item) => {
      const expirationDate = new Date(JSON.parse(item.EXPIRATION_DATE));
      const temp = (expirationDate.getTime() - new Date().getTime()) / 1000;
      setTimeout(() => {
        closeApp();
        alert("Your session has expired, Please login again.");
      }, temp * 1000);
      let draggableDiv = document.getElementById("auto-fill-draggable-div");
      if (!document.getElementById("auto-fill-draggable-div")) {
        draggableDiv = document.createElement("div");
        draggableDiv.setAttribute("id", "auto-fill-draggable-div");
        draggableDiv.innerHTML = `
          <div id="auto-fill-draggable-div-header">
            <div id="bp__menu" class="bp__menu">
              <div class="bp__sub__menu"></div>
              <div class="bp__sub__menu"></div>
              <div class="bp__sub__menu"></div>
            </div>
            <div id="bp-my-dropdown" class="bp__dropdown__content">
              <a id="bp-logout-app">Log out</a>
            </div>
            <p>Click here to move Extension</p>
            <button id="close_cb_x" class="close_cb_x exit">X</button>
          </div>
          <div id="bp-root-app"></div>
        `;
        document.body.appendChild(draggableDiv);
        Draggable.dragElement(
          document.getElementById("auto-fill-draggable-div")
        );
        const app = <App token={item.AUTH_TOKEN} user={item.USER} />;
        ReactDOM.render(app, document.getElementById("bp-root-app"));
        addListenerCloseBtn();
        addListenerMenuBtn();
        addListenerLogout();
      }
    }
  );
}

window.onclick = function (event) {
  if (
    !event.target.matches(".bp__menu") &&
    !event.target.matches(".bp__sub__menu")
  ) {
    let openDropdown = document.getElementById("bp-my-dropdown");
    if (
      openDropdown &&
      openDropdown.classList.contains("bp__dropdown__content__show")
    ) {
      openDropdown.classList.remove("bp__dropdown__content__show");
    }
  }
  if (document.getElementById("auto-fill-draggable-div")) {
    commonFunction.addClassNameForInput();
  } else {
    commonFunction.removeClassNameForInput();
  }
};

function addListenerMenuBtn() {
  document.getElementById("bp__menu").addEventListener("click", () => {
    document
      .getElementById("bp-my-dropdown")
      .classList.toggle("bp__dropdown__content__show");
  });
}

function addListenerLogout() {
  document.getElementById("bp-logout-app").addEventListener("click", () => {
    chrome.storage.sync.clear();
    closeApp();
  });
}

function closeApp() {
  commonFunction.removeClassNameForInput();
  document.getElementById("auto-fill-draggable-div").remove();
}

function addListenerCloseBtn() {
  document.getElementById("close_cb_x").addEventListener("click", () => {
    closeApp();
  });
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  switch (request.todo) {
    case Constants.TODO.TOGGLE_DRAGGABLE_DIV: {
      createDraggableDiv();
      break;
    }
  }
});
