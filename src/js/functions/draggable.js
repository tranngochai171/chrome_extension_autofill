import Constants from "../constants/constant.json";
const draggable = {
  dragElement(elmnt) {
    var pos1 = 0,
      pos2 = 0,
      pos3 = 0,
      pos4 = 0;
    if (document.getElementById(elmnt.id + "-header")) {
      // if present, the header is where you move the DIV from:
      document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;
    } else {
      // otherwise, move the DIV from anywhere inside the DIV:
      elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      const top = elmnt.offsetTop - pos2 < 0 ? 0 : elmnt.offsetTop - pos2;
      let left = elmnt.offsetLeft - pos1 < 0 ? 0 : elmnt.offsetLeft - pos1;
      const limitWidth = window.innerWidth - Constants.WIDTH_OF_DRAGGBALE_DIV;
      if (left > limitWidth) {
        left = limitWidth;
      }
      elmnt.style.top = top + "px";
      elmnt.style.left = left + "px";
    }

    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
  },
};

export default draggable;
