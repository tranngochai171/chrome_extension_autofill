import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://10.0.26.35:7005/",
});

export default axiosInstance;
