import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://10.0.0.86:9001",
});

export default axiosInstance;
