const options = {
  postOption(url, data, AUTH_TOKEN, COUNTRY) {
    return {
      method: "post", //you can set what request you want to be
      crossDomain: true,
      async: true,
      url,
      data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + AUTH_TOKEN,
        country: COUNTRY,
      },
    };
  },
  getOptionCallAPI(method, url, data, AUTH_TOKEN) {
    return {
      method,
      url,
      data,
      headers: {
        Authorization: "Bearer " + AUTH_TOKEN,
      },
    };
  },
};

export default options;
