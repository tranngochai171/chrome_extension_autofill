import Constants from "../constants/constant.json";

const commonFunction = {
  getBtnSelectClassName: (status) => {
    switch (status) {
      case Constants.BTN_STATUS.CURRENT_SELECT:
        return "cb_btn_current_select";
      case Constants.BTN_STATUS.SELECTED:
        return "cb_btn_selected";
      default:
        return "";
    }
  },
  findIndexOfCurrentSelectedBtn: (currentData) => {
    return currentData.findIndex(
      (item) => item.status === Constants.BTN_STATUS.CURRENT_SELECT
    );
  },
  addClassNameForInput: () => {
    let selectElements = document.querySelectorAll("select:not(.bp__select)");
    let inputElements = document.querySelectorAll(
      "input:not(.bp__search):not(.import_btn)"
    );
    let textAreaElements = document.querySelectorAll("textarea");
    let i = 0;
    while (i < selectElements.length) {
      selectElements[i].classList.add("cb_import");
      selectElements[i].classList.add("cb_import_" + i);
      i++;
    }
    for (let j = 0; j < inputElements.length; j++) {
      inputElements[j].classList.add("cb_import");
      inputElements[j].classList.add("cb_import_" + i);
      i++;
    }
    for (let j = 0; j < textAreaElements.length; j++) {
      textAreaElements[j].classList.add("cb_import");
      textAreaElements[j].classList.add("cb_import_" + i);
      i++;
    }
  },
  removeClassNameForInput: () => {
    const inputElements = document.getElementsByClassName("cb_import");
    for (let i = inputElements.length - 1; i >= 0; i--) {
      const inputClassName = commonFunction.getInputClassName(
        inputElements[i].classList
      );
      if (inputClassName) {
        commonFunction.addOrRemoveClassName(
          inputElements[i],
          "",
          inputClassName
        );
      }
      commonFunction.addOrRemoveClassName(inputElements[i], "", "cb_import");
    }
  },
  getInputClassName: (classList) => {
    for (const className of classList) {
      if (/.*cb_import_.*/g.test(className)) {
        return className;
      }
    }
    return "";
  },
  getIndexOfClassNameAlreadyExist: (currentData, classNameInput) => {
    for (let i = 0; i < currentData.length; i++) {
      if (currentData[i].className === classNameInput) {
        return i;
      }
    }
    return -1;
  },
  addOrRemoveClassName: (element, addClass, removeClass) => {
    addClass && element.classList.add(addClass);
    removeClass && element.classList.remove(removeClass);
  },
  mapListToList: (list1, list2) => {
    const tempList1 = JSON.parse(JSON.stringify(list1)); // not have value
    const tempList2 = JSON.parse(JSON.stringify(list2)); // not have className
    for (let i = 0; i < tempList1.length; i++) {
      if (tempList1[i].className) {
        const index = tempList2.findIndex(
          (item) => item.key === tempList1[i].key
        );
        if (index > -1) {
          tempList2[index].status = tempList1[i].status;
          tempList2[index].className = tempList1[i].className;
        }
      }
    }
    return tempList2;
  },
  wrapText: (text) => {
    if (text.length > 14) {
      text = text.slice(0, 5) + ".." + text.slice(-4);
    }
    return text;
  },
  handleAsync: (promise) => {
    return promise
      .then((data) => [data, undefined])
      .catch((error) => Promise.resolve([undefined, error]));
  },
  findValueInOptions: (options, value) => {
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === value) {
        return true;
      }
    }
    return false;
  },
};

export default commonFunction;
